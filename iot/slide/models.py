# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class slide(models.Model):
	nama = models.CharField(max_length=1000)
	konten = models.TextField()
	durasi_tampil = models.CharField(max_length=10)
	image = models.ImageField(upload_to='slide_image',blank=True)

	def __str__(self):
		return "{}".format(self.nama)