# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

from django.core import serializers

from .models import slide

import random

import json


# Create your views here.
def replace_all(text, dic):
	for i, j in dic.items():
		text = text.replace(i, j)
	return text

def index(request):
	total = slide.objects.count()
	acak = random.randint(1,total)
	slide_json = slide.objects.get(id=acak)
	#konten = str(slide_json.konten).replace('"', '\"')
	#escape_json = '[{"model": "slide.slide", "pk": 1, "fields": {"nama": "'+slide_json.nama+'", "konten":'+json.dumps(konten)+', "durasi_tampil": "1", "image": "slide_image/slide_arab.jpg"}}]';
	#return HttpResponse(escape_json.decode('string_escape'));
	d = { "'": "", '"': ''}
	konten = replace_all(slide_json.konten, d)
	escape_json = '[{"model": "slide.slide", "pk": 1, "fields": {"nama": "'+slide_json.nama+'", "konten":"'+konten+'", "durasi_tampil": "1", "image": "slide_image/slide_arab.jpg"}}]';
	return HttpResponse(escape_json);


def create(request):
	post_form = slide()

	if request.method == "GET" :
		slide.objects.create(
				nama = request.GET['nama'],
				konten = request.GET['konten'],
				durasi_tampil = request.GET['durasi_tampil'],
				image = 'image'
			)
	slide_json = serializers.serialize("json", slide.objects.all())
	return HttpResponse(slide_json);


def update(request):
	post_form = slide()

	slide.objects.filter(id=request.GET['id']).update(
				nama = request.GET['nama'],
				konten = request.GET['konten'],
				durasi_tampil = request.GET['durasi_tampil'],
				image = 'image'
			)

	slide_json = serializers.serialize("json", slide.objects.all())
	return HttpResponse(slide_json);


def delete(request):
	slide.objects.filter(id=request.GET['id']).delete()
	slide_json = serializers.serialize("json", slide.objects.all())
	return HttpResponse(slide_json);
	