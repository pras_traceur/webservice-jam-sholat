# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-08-05 13:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='kota',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=1000)),
                ('latitude', models.CharField(max_length=1000)),
                ('longitude', models.CharField(max_length=1000)),
                ('shubuh', models.CharField(max_length=100)),
                ('fajr', models.CharField(max_length=100)),
                ('dzuhur', models.CharField(max_length=100)),
                ('ashar', models.CharField(max_length=100)),
                ('maghrib', models.CharField(max_length=100)),
                ('isya', models.CharField(max_length=100)),
            ],
        ),
    ]
