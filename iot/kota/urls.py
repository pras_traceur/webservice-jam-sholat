from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^$', views.index),
	url(r'^create', views.create),
	url(r'^update', views.update),	
	url(r'^delete', views.delete),
	url(r'^cariId', views.cariId),
	url(r'^cariStatus', views.cariStatus),
	url(r'^editStatus', views.editStatus),
]
