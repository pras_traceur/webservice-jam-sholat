# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django.http import HttpResponse

from django.core import serializers

from .models import kota

# Create your views here.

def index(request):
	kota_json = serializers.serialize("json", kota.objects.all())
	return HttpResponse(kota_json);

def create(request):
	post_form = kota()

	if request.method == "GET" :
		kota.objects.create(
				nama  = request.GET['nama'],
				latitude = request.GET['latitude'],
				longitude = request.GET['longitude'],
				shubuh = request.GET['shubuh'],
				fajr = request.GET['fajr'],
				dzuhur = request.GET['dzuhur'],
				ashar = request.GET['ashar'],
				maghrib = request.GET['maghrib'],
				isya = request.GET['isya'],
				status = request.GET['status'])
	kota_json = serializers.serialize("json", kota.objects.all())
	return HttpResponse(kota_json);


def update(request):
	post_form = kota()

	kota.objects.filter(id=request.GET['id']).update(
				nama  = request.GET['nama'],
				latitude = request.GET['latitude'],
				longitude = request.GET['longitude'],
				shubuh = request.GET['shubuh'],
				fajr = request.GET['fajr'],
				dzuhur = request.GET['dzuhur'],
				ashar = request.GET['ashar'],
				maghrib = request.GET['maghrib'],
				isya = request.GET['isya'],
				status = request.GET['status'])

	kota_json = serializers.serialize("json", kota.objects.all())
	return HttpResponse(kota_json);


def delete(request):
	kota.objects.filter(id=request.GET['id']).delete()
	kota_json = serializers.serialize("json", kota.objects.all())
	return HttpResponse(kota_json);

def cariId(request):
	kotas = kota.objects.filter(id=request.GET['id'])
	kota_json = serializers.serialize("json", kotas)
	return HttpResponse(kota_json);

def cariStatus(request):
	kotas = kota.objects.filter(status=request.GET['status'])
	kota_json = serializers.serialize("json", kotas)
	return HttpResponse(kota_json);

def editStatus(request):
	post_form = kota()
	kota.objects.filter(status='1').update(status = '0',)
	kota.objects.filter(id=request.GET['id']).update(status = request.GET['status'])
	return HttpResponse('oke');
