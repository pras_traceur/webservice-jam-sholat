# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class kota(models.Model):
	nama = models.CharField(max_length=1000)
	latitude = models.CharField(max_length=1000)
	longitude = models.CharField(max_length=1000)
	shubuh = models.CharField(max_length=100)
	fajr = models.CharField(max_length=100)
	dzuhur = models.CharField(max_length=100)
	ashar = models.CharField(max_length=100)
	maghrib = models.CharField(max_length=100)
	isya = models.CharField(max_length=100)
	status = models.CharField(max_length=10)

	def __str__(self):
		return "{}".format(self.nama)