from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^$', views.mobile),
	url(r'^mobile', views.mobile),
	url(r'^native', views.native),
	url(r'^create', views.create),
	url(r'^update', views.update),	
	url(r'^delete', views.delete),	
	url(r'^api', views.api),
	url(r'^getGps', views.getGps),
]