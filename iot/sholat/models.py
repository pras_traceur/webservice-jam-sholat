# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class sholat(models.Model):
	id_madzhab = models.CharField(max_length=11)
	nama_waktu_sholat = models.CharField(max_length=1000)
	jam_sholat = models.CharField(max_length=10)
	durasi_adzan = models.CharField(max_length=5) 
	durasi_iqomah = models.CharField(max_length=5)
	durasi_tarhim = models.CharField(max_length=5)
	durasi_sholat = models.CharField(max_length=5)
	mp3_tarhim = models.CharField(max_length=1000)

	def __str__(self):
		return "{}".format(self.nama_waktu_sholat)
		