# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render


from django.http import HttpResponse

from django.core import serializers

from .models import runtext

# Create your views here.

def index(request):
	runtext_json = serializers.serialize("json", runtext.objects.all())
	return HttpResponse(runtext_json);

def create(request):
	post_form = runtext()

	if request.method == "GET" :
		runtext.objects.create(
				teks  = request.GET['teks']
			)
	runtext_json = serializers.serialize("json", runtext.objects.all())
	return HttpResponse(runtext_json);


def update(request):
	post_form = runtext()

	runtext.objects.filter(id=request.GET['id']).update(
				teks  = request.GET['teks'])

	runtext_json = serializers.serialize("json", runtext.objects.all())
	return HttpResponse(runtext_json);


def delete(request):
	runtext.objects.filter(id=request.GET['id']).delete()
	runtext_json = serializers.serialize("json", runtext.objects.all())
	return HttpResponse(runtext_json);
	