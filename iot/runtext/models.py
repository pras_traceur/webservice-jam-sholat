# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class runtext(models.Model):
	teks = models.CharField(max_length=100000)

	def __str__(self):
		return "{}".format(self.teks)