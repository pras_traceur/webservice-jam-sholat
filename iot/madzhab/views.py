# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django.http import HttpResponse

from django.core import serializers

from .models import madzhab

# Create your views here.

def index(request):
	madzhab_json = serializers.serialize("json", madzhab.objects.all())
	return HttpResponse(madzhab_json);

def create(request):
	post_form = madzhab()

	if request.method == "GET" :
		madzhab.objects.create(
				nama  = request.GET['nama'],
				slug  = request.GET['slug'],
				status = request.GET['status'],
				tune = request.GET['tune'],
				latitude = request.GET['latitude'],
				longitude = request.GET['longitude'],
			)
	madzhab_json = serializers.serialize("json", madzhab.objects.all())
	return HttpResponse(madzhab_json);


def update(request):
	post_form = madzhab()

	madzhab.objects.filter(id=request.GET['id']).update(
				nama  = request.GET['nama'],
				slug  = request.GET['slug'],
				status = request.GET['status'],
				tune = request.GET['tune'],
				latitude = request.GET['latitude'],
				longitude = request.GET['longitude'])

	madzhab_json = serializers.serialize("json", madzhab.objects.all())
	return HttpResponse(madzhab_json);


def delete(request):
	madzhab.objects.filter(id=request.GET['id']).delete()
	madzhab_json = serializers.serialize("json", madzhab.objects.all())
	return HttpResponse(madzhab_json);


def cariId(request):
	madzhabs = madzhab.objects.filter(id=request.GET['id'])
	madzhab_json = serializers.serialize("json", madzhabs)
	return HttpResponse(madzhab_json);


def cariStatus(request):
	madzhabs = madzhab.objects.filter(status=request.GET['status'])
	madzhab_json = serializers.serialize("json", madzhabs)
	return HttpResponse(madzhab_json);

def editStatus(request):
	post_form = madzhab()
	madzhab.objects.filter(status='1').update(status = '0',)
	return HttpResponse('oke');

def LatLong(request):
	post_form = madzhab()

	madzhab.objects.filter(status='1').update(status = '0',)

	madzhab.objects.filter(id=request.GET['id']).update(
		status = request.GET['status'],
		latitude = request.GET['lat'],
		longitude = request.GET['lng'],
		)

	madzhab_json = serializers.serialize("json", madzhab.objects.all())
	return HttpResponse(madzhab_json);