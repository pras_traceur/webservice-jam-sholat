# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class madzhab(models.Model):
	nama = models.CharField(max_length=1000)
	slug = models.CharField(max_length=100, null=True)
	status = models.CharField(max_length=1)
	tune = models.CharField(max_length=100)
	latitude = models.CharField(max_length=1000)
	longitude = models.CharField(max_length=1000)

	def __str__(self):
		return "{}".format(self.nama)