# -*- coding: utf-8 -*-
# Generated by Django 1.11.23 on 2019-10-30 17:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('madzhab', '0002_madzhab_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='madzhab',
            name='latitude',
            field=models.CharField(max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='madzhab',
            name='longitude',
            field=models.CharField(max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='madzhab',
            name='nama',
            field=models.CharField(max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='madzhab',
            name='status',
            field=models.CharField(max_length=1, null=True),
        ),
        migrations.AlterField(
            model_name='madzhab',
            name='tune',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
