# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

from django.core import serializers

from .models import pengumuman

import random

import json


# Create your views here.
def replace_all(text, dic):
	for i, j in dic.items():
		text = text.replace(i, j)
	return text

def index(request):
	pengumuman_json = pengumuman.objects.get(id=1)
	d = { "'": "", '"': ''}
	konten = replace_all(pengumuman_json.konten, d)
	escape_json = '[{"model": "pengumuman.pengumuman", "pk": 1, "fields": {"konten":"'+konten+'"}}]';
	return HttpResponse(escape_json);


def create(request):
	post_form = pengumuman()

	if request.method == "GET" :
		pengumuman.objects.create(konten = request.GET['konten'])

	pengumuman_json = serializers.serialize("json", pengumuman.objects.all())
	return HttpResponse(pengumuman_json);


def update(request):
	post_form = pengumuman()

	pengumuman.objects.filter(id=request.GET['id']).update(konten = request.GET['konten'])

	pengumuman_json = serializers.serialize("json", pengumuman.objects.all())
	return HttpResponse(pengumuman_json);


def delete(request):
	pengumuman.objects.filter(id=request.GET['id']).delete()
	pengumuman_json = serializers.serialize("json", pengumuman.objects.all())
	return HttpResponse(pengumuman_json);
	