# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django.http import HttpResponse

from django.core import serializers

from .models import kajian

# Create your views here.

from datetime import datetime

def index(request):
	myDate = datetime.now()
	Now = myDate.strftime("%Y-%m-%d")
	kajian_json = serializers.serialize("json", kajian.objects.filter(tanggal_kadaluarsa__gte=Now))
	return HttpResponse(kajian_json);


def create(request):
	post_form = kajian()

	if request.method == "GET" :
		kajian.objects.create(
				nama_acara  = request.GET['nama_acara'],
				tema = request.GET['tema'],
				tempat = request.GET['tempat'],
				tanggal_kadaluarsa = request.GET['tanggal_kadaluarsa'],
				waktu_kadaluarsa = request.GET['waktu_kadaluarsa'],
				keterangan = request.GET['keterangan'],
			)

	kajian_json = serializers.serialize("json", kajian.objects.all())
	return HttpResponse(kajian_json);


def update(request):
	post_form = kajian()

	kajian.objects.filter(id=request.GET['id']).update(
				nama_acara  = request.GET['nama_acara'],
				tema = request.GET['tema'],
				tempat = request.GET['tempat'],
				tanggal_kadaluarsa = request.GET['tanggal_kadaluarsa'],
				waktu_kadaluarsa = request.GET['waktu_kadaluarsa'],
				keterangan = request.GET['keterangan'])

	kajian_json = serializers.serialize("json", kajian.objects.all())
	return HttpResponse(kajian_json);


def delete(request):
	kajian.objects.filter(id=request.GET['id']).delete()
	kajian_json = serializers.serialize("json", kajian.objects.all())
	return HttpResponse(kajian_json);
	