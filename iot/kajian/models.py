# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class kajian(models.Model):
	nama_acara = models.CharField(max_length=10000)
	tema = models.CharField(max_length=10000)
	tempat = models.CharField(max_length=10000)
	tanggal_kadaluarsa = models.CharField(max_length=100)
	waktu_kadaluarsa = models.CharField(max_length=100)
	keterangan = models.TextField()

	def __str__(self):
		return "{}".format(self.nama_acara)