# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class setting(models.Model):
	id_produk = models.CharField(max_length=11)
	nama = models.CharField(max_length=1000)
	negara = models.CharField(max_length=100)
	provinsi = models.CharField(max_length=100)
	kabupaten = models.CharField(max_length=100)
	kecamatan = models.CharField(max_length=100)
	alamat = models.CharField(max_length=10000)
	suara = models.CharField(max_length=10)
	volume = models.CharField(max_length=3)
	image = models.ImageField(upload_to='profile_image',blank=True)
	hidup = models.CharField(max_length=10, null=True)
	mati = models.CharField(max_length=10, null=True)
	telepon = models.CharField(max_length=100, null=True)
	username = models.CharField(max_length=100, null=True)
	password = models.CharField(max_length=100, null=True)
	isgps = models.CharField(max_length=10, null=True)

	def __str__(self):
		return "{}".format(self.id_produk)