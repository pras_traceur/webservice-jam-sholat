# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-07-19 20:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('setting', '0003_auto_20190517_0018'),
    ]

    operations = [
        migrations.AddField(
            model_name='setting',
            name='password',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='setting',
            name='telepon',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='setting',
            name='username',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
