# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

from django.core import serializers

from .models import setting

from sholat.models import sholat
from kajian.models import kajian

from madzhab.models import madzhab

from pengumuman.models import pengumuman

# Create your views here.

def index(request):
	settings = setting.objects.get(id=1)
	return HttpResponse('[{"model": "setting.setting", "pk": 1, "fields": {"id_produk": "'+settings.id_produk+'", "nama": "'+settings.nama+'", "negara": "'+settings.negara+'", "provinsi": "'+settings.provinsi+'", "kabupaten": "'+settings.kabupaten+'", "kecamatan": "'+settings.kecamatan+'", "alamat": "'+settings.alamat+'", "suara": "'+settings.suara+'", "volume": "'+settings.volume+'", "image": "profile_image/setting.png", "hidup": "'+settings.hidup+'", "mati": "'+settings.mati+'", "telepon": "'+settings.telepon+'"}}]');

def mobile(request):
	setting_json = serializers.serialize("json", setting.objects.all())
	return HttpResponse(setting_json);

def create(request):
	post_form = setting()

	if request.method == "GET" :
		setting.objects.create(
				id_produk  = request.GET['id_produk'],
				nama = request.GET['nama'],
				negara = request.GET['negara'],
				provinsi = request.GET['provinsi'],
				kabupaten = request.GET['kabupaten'],
				kecamatan = request.GET['kecamatan'],
				alamat = request.GET['alamat'],
				suara = request.GET['suara'],
				volume = request.GET['volume'],
				hidup = request.GET['hidup'],
				mati = request.GET['mati'],
				telepon = request.GET['telepon'],
				username = request.GET['username'],
				password = request.GET['password'],
				isgps = request.GET['isgps']
			)
	setting_json = serializers.serialize("json", setting.objects.all())
	return HttpResponse(setting_json);


def update(request):
	post_form = setting()

	setting.objects.filter(id=request.GET['id']).update(
				id_produk  = request.GET['id_produk'],
				nama = request.GET['nama'],
				negara = request.GET['negara'],
				provinsi = request.GET['provinsi'],
				kabupaten = request.GET['kabupaten'],
				kecamatan = request.GET['kecamatan'],
				alamat = request.GET['alamat'],
				suara = request.GET['suara'],
				volume = request.GET['volume'],
				hidup = request.GET['hidup'],
				mati = request.GET['mati'],
				telepon = request.GET['telepon'],
				username = request.GET['username'],
				password = request.GET['password'],
				isgps = request.GET['isgps']
			)
	setting_json = serializers.serialize("json", setting.objects.all())
	return HttpResponse(setting_json);

def gps(request):
	post_form = setting()

	setting.objects.filter(id=request.GET['id']).update(isgps = request.GET['isgps'])
	setting_json = serializers.serialize("json", setting.objects.all())
	return HttpResponse(setting_json);


def delete(request):
	setting.objects.filter(id=request.GET['id']).delete()
	setting_json = serializers.serialize("json", setting.objects.all())
	return HttpResponse(setting_json);
	
def reset(request):
	#delete kajian
	del_kajian = kajian.objects.all().delete()

	#tambah Kajian
	kajian.objects.create(
				nama_acara  = 'Kajian Mingguan Ust......',
				tema = 'Tema Kajian yang akan di Sampaikan',
				tempat = 'Alamat Masjid Ku Berada',
				tanggal_kadaluarsa = '2050-01-01',
				waktu_kadaluarsa = '00:00',
				keterangan = 'Keterangan Tambahan Tema Kajian yang akan di Sampaikan',
			)

	#setting sholat
	#Shubuh
	sholat.objects.filter(id_madzhab='1').update(jam_sholat = '00:00',durasi_adzan = '03:00', durasi_iqomah = '03:00', durasi_tarhim = '03:00', durasi_sholat = '03:00')
	#Dhuhur
	sholat.objects.filter(id_madzhab='2').update(jam_sholat = '00:00',durasi_adzan = '03:00', durasi_iqomah = '03:00', durasi_tarhim = '03:00', durasi_sholat = '03:00')
	#Ashar
	sholat.objects.filter(id_madzhab='3').update(jam_sholat = '00:00',durasi_adzan = '03:00', durasi_iqomah = '03:00', durasi_tarhim = '03:00', durasi_sholat = '03:00')
	#Maghrib
	sholat.objects.filter(id_madzhab='4').update(jam_sholat = '00:00',durasi_adzan = '03:00', durasi_iqomah = '03:00', durasi_tarhim = '03:00', durasi_sholat = '03:00')
	#Isya
	sholat.objects.filter(id_madzhab='5').update(jam_sholat = '00:00',durasi_adzan = '03:00', durasi_iqomah = '03:00', durasi_tarhim = '03:00', durasi_sholat = '03:00')

	#Setting
	setting.objects.filter(id='1').update(
				id_produk  = '1',
				nama = 'Nama Masjid Ku',
				negara = 'Indonesia',
				provinsi = '-',
				kabupaten = '-',
				kecamatan = '-',
				alamat = 'Alamat Masjid Ku Berada',
				suara = 'true',
				volume = '100',
				hidup = '03:00',
				mati = '22:00',
				telepon = '000000000000',
				username = 'admin',
				password = 'admin',
				isgps = '0'
			)

	#GPS
	madzhab.objects.filter(id=1).update(status = '0',latitude = '0',longitude = '0')
	madzhab.objects.filter(id=2).update(status = '0',latitude = '0',longitude = '0')
	madzhab.objects.filter(id=3).update(status = '0',latitude = '0',longitude = '0')
	madzhab.objects.filter(id=4).update(status = '0',latitude = '0',longitude = '0')
	madzhab.objects.filter(id=5).update(status = '0',latitude = '0',longitude = '0')
	madzhab.objects.filter(id=6).update(status = '0',latitude = '0',longitude = '0')
	madzhab.objects.filter(id=7).update(status = '0',latitude = '0',longitude = '0')
	madzhab.objects.filter(id=8).update(status = '1',latitude = '0',longitude = '0')

	#Reset Pengumuman
	pengumuman.objects.filter(id='1').update(konten = 'Pengumuman Masjid Ku')

	return HttpResponse('{"status": "ok"}');