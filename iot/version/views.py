# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

from django.core import serializers

from .models import version

# Create your views here.

def index(request):
	version_json = serializers.serialize("json", version.objects.all())
	return HttpResponse(version_json);


def create(request):
	post_form = version()

	if request.method == "GET" :
		version.objects.create(
				versi  = request.GET['versi'],
				tanggal = request.GET['tanggal'],
			)

	version_json = serializers.serialize("json", version.objects.all())
	return HttpResponse(version_json);


def update(request):
	post_form = version()

	version.objects.filter(id=request.GET['id']).update(
				versi  = request.GET['versi'],
				tanggal = request.GET['tanggal'])

	version_json = serializers.serialize("json", version.objects.all())
	return HttpResponse(version_json);


def delete(request):
	version.objects.filter(id=request.GET['id']).delete()
	version_json = serializers.serialize("json", version.objects.all())
	return HttpResponse(version_json);