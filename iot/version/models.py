# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class version(models.Model):
	versi = models.CharField(max_length=100)
	tanggal = models.DateField(auto_now=False)

	def __str__(self):
		return "{}".format(self.versi)