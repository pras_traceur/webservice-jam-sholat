# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-04-20 10:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='tema',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('jenis', models.CharField(max_length=1000)),
                ('status', models.CharField(max_length=1)),
            ],
        ),
    ]
