# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class tema(models.Model):
	jenis = models.CharField(max_length=1000)
	status = models.CharField(max_length=1)

	def __str__(self):
		return "{}".format(self.jenis)
		