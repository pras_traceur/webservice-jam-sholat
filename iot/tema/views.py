# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

from django.core import serializers

from .models import tema

# Create your views here.

def index(request):
	tema_json = serializers.serialize("json", tema.objects.all())
	return HttpResponse(tema_json);


def create(request):
	post_form = tema()

	if request.method == "GET" :
		tema.objects.create(
				jenis  = request.GET['jenis'],
				status = request.GET['status'],
			)
	tema_json = serializers.serialize("json", tema.objects.all())
	return HttpResponse(tema_json);


def update(request):
	post_form = tema()

	tema.objects.filter(id=request.GET['id']).update(
				jenis=request.GET['jenis'],
				status=request.GET['status'])

	tema_json = serializers.serialize("json", tema.objects.all())
	return HttpResponse(tema_json);


def delete(request):
	tema.objects.filter(id=request.GET['id']).delete()
	tema_json = serializers.serialize("json", tema.objects.all())
	return HttpResponse(tema_json);
	