# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django.http import HttpResponse

from django.core import serializers

from .models import bahasa

# Create your views here.

def index(request):
	bahasa_json = serializers.serialize("json", bahasa.objects.all())
	return HttpResponse(bahasa_json);

def create(request):
	post_form = bahasa()

	if request.method == "GET" :
		bahasa.objects.create(
				bahasa  = request.GET['bahasa'],
				status = request.GET['status'],
			)
	bahasa_json = serializers.serialize("json", bahasa.objects.all())
	return HttpResponse(bahasa_json);


def update(request):
	post_form = bahasa()

	bahasa.objects.filter(id=request.GET['id']).update(
				bahasa=request.GET['bahasa'],
				status=request.GET['status'])

	bahasa_json = serializers.serialize("json", bahasa.objects.all())
	return HttpResponse(bahasa_json);


def delete(request):
	bahasa.objects.filter(id=request.GET['id']).delete()
	bahasa_json = serializers.serialize("json", bahasa.objects.all())
	return HttpResponse(bahasa_json);