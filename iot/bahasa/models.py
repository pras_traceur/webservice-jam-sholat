# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class bahasa(models.Model):
	bahasa = models.CharField(max_length=200)
	status = models.CharField(max_length=1)

	def __str__(self):
		return "{}".format(self.bahasa)
		